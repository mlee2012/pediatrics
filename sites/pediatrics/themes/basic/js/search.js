function processForm()
{

   //  p&s or cumc radio button search selected
   //  p&s google search
    if ((document.myform.searchtype[0].checked == true) || (document.myform.searchtype[1].checked == true)) 
	{

       if (document.myform.searchtype[0].checked == true) 
	   {
            document.myform.action = 'http://search.cumc.columbia.edu/search';
            document.myform.site.value = "pediatrics";
			document.myform.client.value = "pediatrics";
            document.myform.proxystylesheet.value = "cumc";
            document.myform.q.value = document.myform.searchbox.value;

       }
       // google search on morningside
       else 
	   {
            //alert('site value:' + document.myform.site.value);
            document.myform.action = 'http://search.columbia.edu/search?';
			document.myform.site.value = "cumc";
			document.myform.client.value = "cumc";
            document.myform.proxystylesheet.value = "cumc";
            document.myform.q.value = document.myform.searchbox.value;
        }

       document.myform.method = "GET";

       document.myform.searchtype.disabled = true;
       document.myform.searchbox.disabled = true;
       document.myform.fullname.disabled = true;
    }
    // people search
    else if ( document.myform.searchtype[2].checked == true )
	{

        document.myform.action = 'https://directory.columbia.edu/people/search?';
        document.myform.fullname.value = document.myform.searchbox.value;
        document.myform.elements['filter.searchTerm'].value = document.myform.searchbox.value;
        document.myform.type.value = "lookup";

        // filter out google search terms before transmitting form
        document.myform.client.disabled = true;
        document.myform.site.disabled = true;
        document.myform.proxystylesheet.disabled = true;

    }

   //alert(searchtype[2]);

    document.myform.submit();
    return true;
}

function checkEnter(e)
{ //e is event object passed from function invocation
   var characterCode //literal character code will be stored in this variable
   if(e && e.which){ //if which property of event object is supported (NN4)
       characterCode = e.which //character code is contained in NN4's which property
   }
   else{
       characterCode = e.keyCode //character code is contained in IE's keyCode property
   }

   if(characterCode == 13){ //if generated character code is equal to ascii 13 (if enter key)
       processForm();
       //document.forms[0].submit() //submit the form
       //return false
   }
   else{
       return true
   }
}