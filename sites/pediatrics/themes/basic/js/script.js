/* Author:
*/

/* Filtering for lists modified by Noah Miller from this great tutorial by Noah Hendrix
http://net.tutsplus.com/tutorials/javascript-ajax/using-jquery-to-manipulate-and-filter-data/
 */ 

$(document).ready(function() {

	//default each item to visible
	$('.filterable').addClass('visible');
	
	//overrides CSS display:none property
	//so only users w/ JS will see the
	//filter box
	$('#search').show();
	$('#filter').keyup(function(event) {
		//if esc is pressed or nothing is entered
    if (event.keyCode == 27 || $(this).val() == '') {
			//if esc is pressed we want to clear the value of search box
			$(this).val('');
			
			//we want each row to be visible because if nothing
			//is entered then all rows are matched.
      $('.filterable').removeClass('visible').show().addClass('visible');
    }
		//if there is text, lets filter
		else {
      filter('.filterable', $(this).val());
    }
	});	
});

//filter results based on query
function filter(selector, query) {
	query	=	$.trim(query); //trim white space
  query = query.replace(/ /gi, '|'); //add OR for regex
  
  $(selector).each(function() {
    ($(this).text().search(new RegExp(query, "i")) < 0) ? $(this).hide().removeClass('visible') : $(this).show().addClass('visible');
  });
}
jQuery(document).ready(function($){

// Removes bottom margin of <p> wrapper that gets inserted around images with caption class
	$('p .image-caption-container').each(function() {
		// Make this code do what you want.
		$(this).unwrap();
	});	

});





