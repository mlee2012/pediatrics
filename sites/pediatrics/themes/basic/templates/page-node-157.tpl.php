<!doctype html>
	<!--[if lt IE 7]>      <html class="ie6"> <![endif]-->
	<!--[if IE 7]>         <html class="ie7"> <![endif]-->
	<!--[if IE 8]>         <html class="ie8"> <![endif]-->
	<!--[if gt IE 8]><!--> <html>         <!--<![endif]--><head>
	<title><?php print $head_title; ?> | Columbia University College of Physicians & Surgeons</title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <!--[if lte IE 6]><link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . path_to_theme(); ?>/css/ie6.css" /><![endif]-->
    <!--[if IE 7]><link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . path_to_theme(); ?>/css/ie7.css" /><![endif]-->
    <!--[if IE 8]><link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . path_to_theme(); ?>/css/ie8.css" /><![endif]-->
<script type="text/javascript" src="<?php print $base_path . path_to_theme(); ?>/js/libs/modernizr-2.5.3.min.js"></script>

</head>
<body class="<?php print $body_classes; ?> wide">
<div id="skip">
      <a href="#content"><?php print t('Skip to Content'); ?></a>
      <?php if (!empty($primary_links) || !empty($secondary_links)): ?>
        <a href="#navigation"><?php print t('Skip to Navigation'); ?></a>
      <?php endif; ?>
</div>
<div class="navwrapper">
<nav class="metanav">
	<ul>
			<li><a href="http://www.cumc.columbia.edu/">CUMC Home</a></li>
			<li><a href="http://www.columbia.edu/">Columbia University</a></li>
			<li><a href="https://directory.columbia.edu/people/search?">Find People</a></li>
			<li><a href="http://www.cumc.columbia.edu/map">Map</a></li>

	</ul>
            <?php print $search_area; ?>

</nav>
</div>
<!-- ______________________ HEADER _______________________ -->
<div class="wrapperheader">
	<header class="pageheader"> 
		<hgroup> 
			<a href="http://ps.columbia.edu" target="blank"> 
				<h1 class="ir pnslogo">
					Columbia University College of Physicians and Surgeons Home 
				</h1>
			</a> 
			<a href="/pediatrics"> 
				<h2 class="orgname">
					<?php print $site_name; ?>
				</h2>
			</a> 
		</hgroup> 
	</header> 
</div>
      <div class="wrappermain clearfix">
		  <nav class="primarynav">
				<?php if (!empty($primary_links)){ print theme('links', $primary_links, array('id' => 'primary', 'class' => 'links main-menu')); } ?></nav>
		  </nav>

      <?php // Uncomment to add the search box.// print $search_box; ?>
<!-- /header -->

<!-- ______________________ MAIN _______________________ -->
 <div id="maincontent" class="clearfix" role="main">
	<hgroup>
<?php if ($title): ?>
	<?php if(strstr($node->path, '/faculty') && strstr($node->path, 'division/') ): 
			$newstr = $node->path;
			$patterns = array("division","faculty", "/"); 
			$newdivname = str_replace($patterns, "", str_replace("-", " ", $newstr));
	?>
	<h2 class="div-faculty">
	<?php if(strstr($newdivname, "nd ")):
			 echo strtotitle($newdivname);
    ?>
	<?php else: ?>
		<?php print t($newdivname); ?>
	<?php endif; ?>
</h2>
<?php endif; ?><h1 class="title"><?php print $title; ?></h1>
<?php endif; ?>
        <?php if (strstr($node->path, 'division')): ?>
            <?php if(isset($node->field_director) && !($node->field_director[0]["value"] == "")): ?> 
            	<a href="" id="togglecontact" value="Toggle Contact Info" name="togglecontact">Information/Contact</a>
			<? endif; ?>
        <?php endif; ?>
	</hgroup>
	
    <?php if ($breadcrumb || $title || $mission || $messages || $help || $tabs): ?>
    	<?php print $breadcrumb; ?>
    <?php endif; ?>
   
   <div id="tabs">
  
   
      
  
   <ul class="pcnav">
    <li><a href="#tab-1">View Conditions</a></li>
    <li><a href="#tab-2">View Divisions</a></li>
    <li id="search">
        <label for="filter">Type a condition to filter results</label> <input type="text" name="filter" value="" id="filter" />
    </li>
   </ul>

    <section id="tab-1">
        
<ul class="pclisting">
	<li class="filterable"><a href="/pediatrics/patient-care/gastroenterology-hepatology-and-nutrition">Abdominal pain</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/infectious-diseases">AIDS</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/allergy-and-immunology">Allergy and Immunology</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Anemia</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/pulmonology">Asthma</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/rheumatology">Arthritis</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/clinical-genetics">Birth defects</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/nephrology">Bladder problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Bleeding problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/cardiology">Blood pressure problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Blood problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Bone marrow transplant</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/neurology">Brain tumors</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/pulmonology">Breathing problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Cancer</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/neurology">Cerebral palsy</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/cardiology">Cholesterol problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/clinical-genetics">Chromosome disorders</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/infectious-diseases">Chronic fatigue</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/infectious-diseases">Chronic infections</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/gastroenterology-hepatology-and-nutrition">Colitis</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/rheumatology">Collagen vascular disorders</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/neonatology-and-perinatology">Congenital diaphragmatic hernia</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/cardiology">Congenital heart problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/gastroenterology-hepatology-and-nutrition">Constipation</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/child-and-adolescent-health">Contraception</a></li>
</ul>	
<ul class="pclisting">	<li class="filterable"><a href="/pediatrics/patient-care/pulmonology">Cystic fibrosis</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/allergy-and-immunology">Desensitization shots</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/child-and-adolescent-health">Development</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/neurology">Developmental delay</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/endocrinology">Diabetes</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/nephrology">Dialysis</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/gastroenterology-hepatology-and-nutrition">Diarrhea</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/clinical-genetics">Down syndrome</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/cardiology">ECHO</a></li>
	
	<li class="filterable"><a href="/pediatrics/patient-care/cardiology">EKG</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/nephrology">Electrolyte problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/emergency-medicine">Emergencies</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/cardiology">Enlarged heart</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/neurology">Epilepsy</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/gastroenterology-hepatology-and-nutrition">Failure to thrive</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/clinical-genetics">Family risk assessment</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/child-and-adolescent-health">Feeding issues</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/endocrinology">Fertility issues</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/allergy-and-immunology">Food Allergies</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/clinical-genetics">Genetic disorders</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/endocrinology-">Growth problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/neurology">Headaches</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/cardiology">Heart failure</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/cardiology<span-class="c3">Heart murmurs</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/cardiology">Heart problems</a></li>

<li class="filterable"><a href="/pediatrics/patient-care/cardiology">Heart transplant</a></li>

</ul>	
<ul class="pclisting">		<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Hemophilia</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/gastroenterology-hepatology-and-nutrition">Hepatitis</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/clinical-genetics">Hereditary diseases</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Immune problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/child-and-adolescent-health">Immunizations</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/child-and-adolescent-health">Infant care</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/rheumatology">Joint problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/rheumatology">Joint swelling</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/nephrology">Kidney failure</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/nephrology">Kidney problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/nephrology">Kidney transplants</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Leukemia</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/gastroenterology-hepatology-and-nutrition">Liver problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/pulmonology">Lung problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/rheumatology">Lupus</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/infectious-diseases">Lyme disease</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Lymph node problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Lymphoma</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Masses</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/neonatology-and-perinatology">Meconium</a></li>

	<li class="filterable"><a href="/pediatrics/patient-care/clinical-genetics">Metabolic disorders</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/neurology">Migraines</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/neonatology-and-perinatology">Newborn problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/gastroenterology-hepatology-and-nutrition">Nutrition</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/infectious-diseases">Persistent fever</a></li>

	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Platelet disorders</a></li>
</ul>	
<ul class="pclisting">	<li class="filterable"><a href="/pediatrics/patient-care/clinical-genetics">Prenatal screening</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/endocrinology">Puberty</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/child-and-adolescent-health">Rashes</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/neonatology-and-perinatology">Respiratory distress</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/cardiology">Rhythm problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/neurology">Seizures</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/child-and-adolescent-health">Sexually transmitted diseases</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/pulmonology">Shortness of breath</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Sickle cell anemia</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/allergy-and-immunology">Skin testing</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/pulmonology">Sleep apnea</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/endocrinology">Thyroid issues</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/emergency-medicine">Trauma</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/infectious-diseases">Travel advice</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">Tumors: brain, bone, liver, kidney</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/nephrology">Urine problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/infectious-diseases">Vaccines</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/infectious-diseases">Viral illnesses</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/neurology">Vision problems</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/gastroenterology-hepatology-and-nutrition">Weight loss</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/adolescent-medicine">Well-adolescent care</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/child-and-adolescent-health">Well-child care</a></li>
	<li class="filterable"><a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation">White blood cell disorders</a></li>
</ul>
</section>
    <section id="tab-2">
    <a href="/pediatrics/patient-care/allergy-and-immunology" class="pcdivisions filterable">
	<ul>
		<li class="heading">
			Allergies <span class="divname">(Division of Allergy and Immunology)</span>
		</li>
		<li>
			Desensitization shots
		</li>
		<li>
			Food Allergy and Immunology
		</li>
		<li>
			Rashes
		</li>
		<li>
			Skin testing
		</li>
	</ul>
</a>
<a href="/pediatrics/patient-care/rheumatology " class="pcdivisions filterable">
	<ul>
		<li class="heading">
			Arthritis <span class="divname">(Division of Rheumatology)</span>
		</li>
		<li>
			Arthritis
		</li>
		<li>
			Joint problems
		</li>
		<li>
			Joint swelling
		</li>
	
	</ul>
</a>
<a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation" class="pcdivisions filterable">
	<ul>
		<li class="heading">
			Blood <span class="divname">(Division of Hematology, Oncology, and Stem Cell Transplantation)</span>
		</li>
		<li>
			Anemia
		</li>
		<li>
			Bleeding problems
		</li>
		<li>
			Blood problems
		</li>
		<li>
			Hemophilia
		</li>
		<li>
			Platelet disorders
		</li>
		<li>
			Sickle cell anemia
		</li>
		<li>
			White blood cell disorders
		</li>
	</ul>
</a>
<a href="/pediatrics/patient-care/hematology-oncology-and-stem-cell-transplantation" class="pcdivisions filterable clear">
	<ul>
		<li class="heading">
			Cancer <span class="divname">(Division of Hematology, Oncology, and Stem Cell Transplantation)</span>
		</li>
		<li>
			Bone marrow transplant
		</li>
		<li>
			Cancer
		</li>
		<li>
			Leukemia
		</li>
		<li>
			Lymph node problems
		</li>
		<li>
			Lymphoma
		</li>
		<li>
			Masses
		</li>
		<li>
			Tumors: brain, bone, liver, kidney
		</li>
		<li>
			Immune problems
		</li>
	</ul>
</a>
<a href="/pediatrics/patient-care/endocrinology" class="pcdivisions filterable">
	<ul>
		<li class="heading">
			Diabetes <span class="divname">(Division of Endocrinology)</span>
		</li>
	</ul>
</a>
<a href="/pediatrics/patient-care/emergency-medicine" class="pcdivisions filterable">
	<ul>
		<li class="heading">
			Emergencies <span class="divname">(Division of Emergency Medicine)</span>
		</li>
		<li>
			Emergencies
		</li>
		<li>
			Trauma
		</li>
	</ul>
</a>
<a href="/pediatrics/patient-care/gastroenterology-hepatology-and-nutrition" class="pcdivisions filterable clear">
	<ul>
		<li class="heading">
			Gastrointestinal <span class="divname">(Division of Gastroenterology, Hepatology, and Nutrition)</span>
		</li>
		<li>
			Abdominal pain
		</li>
		<li>
			Colitis
		</li>
		<li>
			Constipation
		</li>
		<li>
			Diarrhea
		</li>
		<li>
			Failure to thrive
		</li>
		<li>
			Hepatitis
		</li>
		<li>
			Liver problems
		</li>
		<li>
			Nutrition
		</li>
		<li>
			Weight loss
		</li>
	</ul>
</a>
<a href="/patient-care/child-and-adolescent-health" class="pcdivisions filterable">
	<ul>
		<li class="heading">
			General Child and Adolescent Health <span class="divname">(Division of Child and Adolescent Health)</span>
		</li>
		<li>
			Contraception
		</li>
		<li>
			Development
		</li>
		<li>
			Feeding issues
		</li>
		<li>
			Growth problems
		</li>
		<li>
			Immunizations
		</li>
		<li>
			Infant care
		</li>
		<li>
			Sexually transmitted diseases
		</li>
		<li>
			Well-adolescent care
		</li>
		<li>
			Well-child care
		</li>
	</ul>
</a>
<a href="/pediatrics/patient-care/clinical-genetics" class="pcdivisions filterable">
	<ul>
		<li class="heading">
			Genetics <span class="divname">(Division of Genetics)</span>
		</li>
		<li>
			Birth defects
		</li>
		<li>
			Chromosome disorders
		</li>
		<li>
			Down syndrome
		</li>
		<li>
			Family risk assessment
		</li>
		<li>
			Genetic disorders
		</li>
		<li>
			Hereditary diseases
		</li>
		<li>
			Metabolic disorders
		</li>
		<li>
			Prenatal screening
		</li>
	</ul>
</a>
<a href="/patient-care/endocrinology" class="pcdivisions filterable clear">
	<ul>
		<li class="heading">
			Glandular <span class="divname">(Division of Endocrinology)</span>
		</li>
		<li>
			Diabetes
		</li>
		<li>
			Fertility issues
		</li>
		<li>
			Growth problems
		</li>
		<li>
			Puberty
		</li>
		<li>
			Thyroid issues
		</li>
	</ul>
</a>
<a href="/pediatrics/patient-care/cardiology" class="pcdivisions filterable">
	<ul>
		<li class="heading">
			Heart <span class="divname">(Division of Cardiology)</span>
		</li>
		<li>
			Blood pressure problems
		</li>
		<li>
			Cholesterol problems 
		</li>
		<li>
			Congenital heart problems
		</li>
		<li>
			ECHO
		</li>
		<li>
			EKG
		</li>
		<li>
			Enlarged heart
		</li>
		<li>
			Heart failure
		</li>
		<li>
			Heart murmurs 
		</li>
		<li>
			Heart problems
		</li>
		<li>
			Heart transplant
		</li>
		<li>
			Rhythm problems
		</li>
	</ul>
</a>
<a href="/pediatrics/patient-care/rheumatology" class="pcdivisions filterable">
	<ul>
		<li class="heading">
			Immune System <span class="divname">(Division of Rheumatology)</span>
		</li>
		<li>Lupus</li>
	</ul>
</a>
<a href="/pediatrics/patient-care/infectious-diseases" class="pcdivisions filterable clear">
	<ul>
		<li class="heading">
			Infections <span class="divname">(Division of Infectious Diseases)</span>
		</li>
		<li>
			AIDS
		</li>
		<li>
			Chronic fatigue
		</li>
		<li>
			Chronic infections
		</li>
		<li>
			Lyme disease
		</li>
		<li>
			Immunizations
		</li>
		<li>
			Persistent fever
		</li>
		<li>
			Rashes
		</li>
		<li>
			Travel advice
		</li>
		<li>
			Vaccines
		</li>
		<li>
			Viral illnesses
		</li>
	</ul>
</a>
<a href="/pediatrics/patient-care/nephrology" class="pcdivisions filterable">
	<ul>
		<li class="heading">
			Kidney <span class="divname">(Division of Nephrology)</span>
		</li>
		<li>
			Bladder problems
		</li>
		<li>
			Blood pressure problems
		</li>
		<li>
			Dialysis 
		</li>
		<li>
			Electrolyte problems
		</li>
		<li>
			Kidney failure
		</li>
		<li>
			Kidney problems
		</li>
		<li>
			Kidney transplants
		</li>
		<li>
			Urine problems
		</li>
	</ul>
</a>
<a href="/patient-care/gastroenterology-hepatology-and-nutrition" class="pcdivisions filterable">
	<ul>
		<li class="heading">
			Liver <span class="divname">(Division of Gastroenterology)</span>
		</li>
		<li>Liver problems</li>
		<li>Hepatitis</li>
	</ul>
</a>
<a href="/pediatrics/patient-care/pulmonology" class="pcdivisions filterable clear">
	<ul>
		<li class="heading">
			Lung <span class="divname">(Division of Pulmonology)</span>
		</li>
		<li>
			Asthma
		</li>
		<li>
			Breathing problems
		</li>
		<li>
			Cystic Fibrosis
		</li>
		<li>
			Lung problems
		</li>
		<li>
			Shortness of breath
		</li>
		<li>
			Sleep apnea
		</li>
	</ul>
</a>
<a href="/pediatrics/patient-care/neurology " class="pcdivisions filterable">
	<ul>
		<li class="heading">
			Nervous System <span class="divname">(Division of Neurology)</span>
		</li>
		<li>
			Cerebral palsy
		</li>
		<li>
			Epilepsy
		</li>
		<li>
			Headaches
		</li>
		<li>
			Seizures
		</li>
		<li>
			Vision problems
		</li>
	</ul>
</a>
<a href="/pediatrics/patient-care/neonatology-and-perinatology" class="pcdivisions filterable">
	<ul>
		<li class="heading">
			Newborn <span class="divname">(Division of Neonatology and Perinatology)</span>
		</li>
		<li>
			Congenital diaphragmatic hernia
		</li>
		<li>
			Meconium
		</li>
		<li>
			Newborn problems
		</li>
		<li>
			Respiratory distress
		</li>
	</ul>
</a>
</section>
 </div>

    <div id="content-area">
    	<?php print $content; ?>
    </div> <!-- /#content-area -->
   <!-- 
    <?php if ($right): ?>
      <div id="sidebar-second" class="column sidebar second">
        <div id="sidebar-second-inner" class="inner">
          <?php print $right; ?>
        </div>
      </div>
    <?php endif; ?>  /sidebar-second -->
    </div> 
  </div>
</div>
<!-- /main -->
<!-- ______________________ FOOTER _______________________ -->
<div class="wrapperfooter">
	<footer>
		<?php if(!empty($footer_message) || !empty($footer_block)): ?>
        <div id="footer">
          <?php print $footer_message; ?>
          <?php print $footer_block; ?>
        </div> <!-- /footer -->
      <?php endif; ?>
	</footer>
	<nav>
	</nav>
</div>
<!-- JavaScript at the bottom for fast page loading -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php print $base_path . path_to_theme(); ?>/js/libs/slides.min.jquery.js"></script>
<script type="text/javascript" src="<?php print $base_path . path_to_theme(); ?>/js/script.js"></script>
<script type="text/javascript" src="http://use.typekit.com/saw7umj.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<!-- scripts concatenated and minified via build script -->
<script type="text/javascript">
$(document).ready(function() 
{ 
	$(".group-contactinfo").hide();
	$('#togglecontact').click(function(){
	$('.group-contactinfo').fadeToggle("slow", "swing");
		return false;
		});
		});
</script>
<script>
	$(document).ready(function(){
$('#tabs section').hide();
$('#tabs section:first').show();
$('#tabs ul li:first').addClass('active');
 
$('#tabs ul.pcnav li a').click(function(){
$('#tabs ul.pcnav li').removeClass('active');
$(this).parent().addClass('active');
var currentTab = $(this).attr('href');
$('#tabs section').hide();
$(currentTab).fadeIn(400).show();
return false;
});
});
	</script>
    
  <!-- end scripts -->

  <!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
       mathiasbynens.be/notes/async-analytics-snippet -->
  <script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>

<?php print $closure; ?>
</body>
</html>