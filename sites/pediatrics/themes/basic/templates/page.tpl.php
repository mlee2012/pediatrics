<!doctype html>
	<!--[if lt IE 7]>      <html class="ie6"> <![endif]-->
	<!--[if IE 7]>         <html class="ie7"> <![endif]-->
	<!--[if IE 8]>         <html class="ie8"> <![endif]-->
	<!--[if gt IE 8]><!--> <html>         <!--<![endif]-->
    <head>
    	<title><?php print $head_title; ?> | Columbia University College of Physicians & Surgeons</title>
    <?php print $head; ?>
    <?php print $styles; ?>
  	

<script type="text/javascript" src="<?php print $base_path . path_to_theme(); ?>/js/libs/modernizr-2.5.3.min.js"></script>
   <?php print $scripts; ?>
</head>
<body class=" ">
<div id="skip">
      <a href="#content"><?php print t('Skip to Content'); ?></a>
      <?php if (!empty($primary_links) || !empty($secondary_links)): ?>
        <a href="#navigation"><?php print t('Skip to Navigation'); ?></a>
      <?php endif; ?>
</div>
<div class="navwrapper">
<nav class="metanav">
	<ul>
			<li><a href="http://www.cumc.columbia.edu/">CUMC Home</a></li>
			<li><a href="http://www.columbia.edu/">Columbia University</a></li>
			<li><a href="http://www.cumc.columbia.edu/about/find-people">Find People</a></li>
			<li><a href="http://www.cumc.columbia.edu/map">Map</a></li>

	</ul>
            <?php print $search_area; ?>

</nav>
</div>
<!-- ______________________ HEADER _______________________ -->
	<div class="wrapperheader">
	<header class="pageheader"> 
		<hgroup> 
			<a href="http://ps.columbia.edu" target="blank"> 
				<h1 class="ir pnslogo">
					Columbia University College of Physicians and Surgeons Home 
				</h1>
			</a> 
			<a href="/pediatrics"> 
				<h2 class="orgname">
					<?php print $site_name; ?>
				</h2>
			</a> 
		</hgroup> 
	</header> 
</div>
  <!-- /header -->
<!-- ______________________ BODY OF PAGE (INCLUDING NAV) _______________________ -->
  
      <div class="wrappermain">
		  <nav class="primarynav">
				<?php if (!empty($primary_links)){ print theme('links', $primary_links, array('id' => 'primary', 'class' => 'links main-menu')); } ?></nav>
		  </nav>

      <?php // Uncomment to add the search box.// print $search_box; ?>


<!-- ______________________ MAIN _______________________ -->
 <?php if ($left): ?>
	 <nav class="secondarynav">  
             <?php print $left; ?>
              <?php if(strstr($node->path, 'division/') && !strstr($node->path, '/patient-care')): ?>
<?php if(!strstr($node->path, '/biomathematics') && !strstr($node->path, '/molecular')): ?>
<?php 
$div_name = str_replace("division/", "", $node->path); 
$new_div = explode("/", $div_name);
?>
<div class="patientcarefeature"><a href="/pediatrics/patient-care/<?php echo $new_div[0]; ?>">
<h2>Patient Care</h2>
Learn about<br />
our patient<br />
services</a></div>
			 <?php endif; ?> <?php endif; ?>
     </nav>
     
<?php endif; ?>
<div id="maincontent" role="main" class="clearfix">
 
 
	<hgroup>
<?php if ($title): ?>
	<?php // echo $title;
	if(strstr($node->path, 'division/') ): 
	echo '<link type="text/css" rel="stylesheet" media="all" href="'.$base_path.path_to_theme().'/css/extstyle.css" />';
			$newstr = $node->path;
			$patterns = array("division","faculty", "/", "contact us", "contact", "research", "residency fellowship", "ways to give", "patient care", "related links", "trainees"); 
			$newdivname = str_replace($patterns, "", str_replace("-", " ", $newstr));
	?>
	<h2 class="div-faculty"><?php if(strstr($newdivname, 'gastroenterology')) print "Gastroenterology, Hepatology and Nutrition"; elseif(strstr($newdivname, 'hematology')) print "Hematology, Oncology, and Stem Cell Transplantation"; else print strtotitle($newdivname); ?></h2>
<?php endif; ?><h1 class="title"><?php print $title; ?></h1>
<?php endif; ?>
        <?php if (strstr($node->path, 'division')): ?>
            <?php if(isset($node->field_director) && !($node->field_director[0]["value"] == "")): ?> 
            	<a href="" id="togglecontact" value="Toggle Contact Info" name="togglecontact">Information/Contact</a>
			<? endif; ?>
        <?php endif; ?>
	</hgroup>
	<?php if ($breadcrumb || $title || $mission || $messages || $help || $tabs): ?>
    	<?php print $breadcrumb; ?>
    <?php endif; ?>
    <div id="content-area">
    <?php  if (strstr($node->path, '/faculty') && strstr($node->path, 'division/')): ?>
	<?php else:?>
    	<?php print $sidebar; ?>
	<?php  endif; ?>
    	<?php print $views_block; ?><?php print $content; ?>
        
    </div> <!-- /#content-area -->
   <!-- 
    <?php if ($right): ?>
      <div id="sidebar-second" class="column sidebar second">
        <div id="sidebar-second-inner" class="inner">
          <?php print $right; ?>
        </div>
      </div>
    <?php endif; ?>  /sidebar-second -->
    </div> 
  </div>
</div>
<!-- /main -->
<!-- ______________________ FOOTER _______________________ -->
<div class="wrapperfooter">
	<footer>
		<?php if(!empty($footer_message) || !empty($footer_block)): ?>
        <div id="footer">
          <?php print $footer_message; ?>
          <?php print $footer_block; ?>
        </div> <!-- /footer -->
      <?php endif; ?>
	</footer>
	<nav>
	</nav>
</div>


<!-- JavaScript at the bottom for fast page loading -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php print $base_path . path_to_theme(); ?>/js/libs/slides.min.jquery.js"></script>
<script type="text/javascript" src="http://use.typekit.com/saw7umj.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<!-- scripts concatenated and minified via build script -->
<script type="text/javascript">
$(document).ready(function() 
{ 
	$(".group-contactinfo").hide();
	
	$('#togglecontact').click(function(){
	$('.group-contactinfo').fadeToggle("slow", "swing");
		return false;
		});
		
	$('.field-field-close').click(function(){
	$('.group-contactinfo').fadeToggle("slow", "swing");
		return false;
		});	
		});

</script>


<script>
		$(function(){
			$('#slides').slides({
				preload: true,
				generatePagination: false,
				play: 4000
			});
		});
	</script>
 <script>
  $(document).ready(function() {
    $(".accordion").accordion({ 
		collapsible: true,
		active: false,
		autoHeight: false,
		header: 'h3'
	  });
  });
  </script>
    
  <!-- end scripts -->

  <!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
       mathiasbynens.be/notes/async-analytics-snippet -->
  <script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>

<?php print $closure; ?>
</body>
</html>