<!doctype html>
	<!--[if lt IE 7]>      <html class="ie6"> <![endif]-->
	<!--[if IE 7]>         <html class="ie7"> <![endif]-->
	<!--[if IE 8]>         <html class="ie8"> <![endif]-->
	<!--[if gt IE 8]><!--> <html>         <!--<![endif]-->
  <head>
    
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <!--[if lte IE 6]><link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . path_to_theme(); ?>/css/ie6.css" /><![endif]-->
    <!--[if IE 7]><link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . path_to_theme(); ?>/css/ie7.css" /><![endif]-->
    <!--[if IE 8]><link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . path_to_theme(); ?>/css/ie8.css" /><![endif]-->

<script type="text/javascript" src="<?php print $base_path . path_to_theme(); ?>/js/libs/modernizr-2.5.3.min.js"></script>
 <?php print $scripts; ?>
  </head>

<body class="homepage <?php print $body_classes; ?> wide">
<div id="skip">
      <a href="#content"><?php print t('Skip to Content'); ?></a>
      <?php if (!empty($primary_links) || !empty($secondary_links)): ?>
        <a href="#navigation"><?php print t('Skip to Navigation'); ?></a>
      <?php endif; ?>
</div>
<div class="navwrapper">
<nav class="metanav">
	<ul>
			<li><a href="http://www.cumc.columbia.edu/">CUMC Home</a></li>
			<li><a href="http://www.columbia.edu/">Columbia University</a></li>
			<li><a href="http://www.cumc.columbia.edu/about/find-people">Find People</a></li>
			<li><a href="http://www.cumc.columbia.edu/map">Map</a></li>

	</ul>
            <?php print $search_area; ?>

</nav>
</div>

    <!-- ______________________ HEADER _______________________ -->

<div class="wrapperheader">
	<header class="pageheader"> 
		<hgroup> 
			<a href="http://ps.columbia.edu" target="blank"> 
				<h1 class="ir pnslogo">
					Columbia University College of Physicians and Surgeons Home 
				</h1>
			</a> 
			<a href="/pediatrics"> 
				<h2 class="orgname">
					<?php print $site_name; ?>
				</h2>
			</a> 
		</hgroup> 
	</header> 
</div>


      <div class="wrappermain clearfix">
		  <nav class="primarynav">
				<?php if (!empty($primary_links)){ print theme('links', $primary_links, array('id' => 'primary', 'class' => 'links main-menu')); } ?></nav>
		  </nav>

  <div id="maincontent" role="main">
	<?php print $slideshow; ?>
  </div>
	<section class="midpage">
	<section class="featureboxes residency">
	<h3>Welcome</h3>
	<h2>Message from the Chair</h2>
	<iframe class="video" src="http://www.youtube.com/embed/fTnKw-Pd-6s" frameborder="0" allowfullscreen></iframe>	
	<h3><a href="about-us">Read more from Dr. Lawrence Stanberry</a>
	</h3>
	</section>
	<section class="featureboxes globalhealth">
	<h3>Education and Training</h3>
	<h2>Residency</h2>
	<iframe class="video" src="http://www.youtube.com/embed/cYG1os7Pbo8" frameborder="0" allowfullscreen></iframe>
	<h3><a href="education-training/residency">Find out more about our residency program</a></h3>
	</section>
	<section class="featureboxes newsandevents">
	<h3>About Us</h3>
	<h2>News &amp; Events</h2>
	
<section>
	<?php print $right; ?>
	<h3><a href="about-us/news">View our previous News and Events Archive</a></h3>
	</section>
	</section>
  </div>
</div>
  </div>
  
</div>
      <!-- /main -->


      <!-- ______________________ FOOTER _______________________ -->
<div class="wrapperfooter">
	<footer>
		<?php if(!empty($footer_message) || !empty($footer_block)): ?>
        <div id="footer">
          <?php print $footer_message; ?>
          <?php print $footer_block; ?>
        </div> <!-- /footer -->
      <?php endif; ?>
	</footer>
	<nav>
	</nav>
</div>
      
  <!-- JavaScript at the bottom for fast page loading -->

 

 <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php print $base_path . path_to_theme(); ?>/js/libs/slides.min.jquery.js"></script>
<script type="text/javascript" src="http://use.typekit.com/saw7umj.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<!-- scripts concatenated and minified via build script -->
  <script type="text/javascript">
	     $(document).ready(function() { 
	 	   $(".group-contactinfo").hide();
		   $('#togglecontact').click(function(){
			 $('.group-contactinfo').fadeToggle("slow", "swing");
			 return false;
		   });
		 });




	</script>
  
   <script>
		$(function(){
			$('#slides').slides({
				preload: true,
				generatePagination:true,
				crossfade: true,
				play: 6000
			});
		});
	</script>
    
  <!-- end scripts -->

  <!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
       mathiasbynens.be/notes/async-analytics-snippet -->
  
  
  
  
  
  <script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>

    <?php print $closure; ?>
  </body>
</html>
