<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<head>
	<title><?php print $head_title; ?> | Columbia University College of Physicians & Surgeons</title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <!--[if lte IE 6]><link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . path_to_theme(); ?>/css/ie6.css" /><![endif]-->
    <!--[if IE 7]><link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . path_to_theme(); ?>/css/ie7.css" /><![endif]-->
    <!--[if IE 8]><link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . path_to_theme(); ?>/css/ie8.css" /><![endif]-->
</head>
<body class="<?php print $body_classes; ?>">
<div id="skip">
      <a href="#content"><?php print t('Skip to Content'); ?></a>
      <?php if (!empty($primary_links) || !empty($secondary_links)): ?>
        <a href="#navigation"><?php print t('Skip to Navigation'); ?></a>
      <?php endif; ?>
</div>
<nav class="metanav">
	<ul>
			<li><a href="">CUMC Home</a></li>
			<li><a href="">Columbia University</a></li>
			<li><a href="">Find People</a></li>
			<li><a href="">Map</a></li>
	</ul>
</nav>
<!-- ______________________ HEADER _______________________ -->
	   <div class="wrapperheader">
		  <header class="pageheader">
			<hgroup>
				<h1 class="ir pnslogo">Columbia University College of Physicians and Surgeons Home</h1>
				<h2 class="orgname"><?php print $site_name; ?></h2>
			</hgroup>
		  </header>
	  </div>
      <div class="wrappermain clearfix">
		  <nav class="primarynav">
				<?php if (!empty($primary_links)){ print theme('links', $primary_links, array('id' => 'primary', 'class' => 'links main-menu')); } ?></nav>
		  </nav>
<?php if ($left): ?>
	 <nav class="secondarynav">  
             <?php print $left; ?>	
     </nav>
<?php endif; ?>
      <?php // Uncomment to add the search box.// print $search_box; ?>
<!-- /header -->

<!-- ______________________ MAIN _______________________ -->
 <div id="maincontent" role="main">
	<hgroup>
<?php if ($title): ?>
	<?php if(strstr($node->path, 'division/') ): 
			$newstr = $node->path;
			$patterns = array("division","faculty", "/", "contact us", "research", "residency fellowship", "ways to give", "patient care", "related links", "trainees"); 
			$newdivname = str_replace($patterns, "", str_replace("-", " ", $newstr));
	?>
	<h2 class="div-faculty"><?php print strtotitle($newdivname); ?></h2>
<?php endif; ?><h1 class="title"><?php print $title; ?></h1>
<?php endif; ?>
        <?php if (strstr($node->path, 'division')): ?>
            <?php if(isset($node->field_director) && !($node->field_director[0]["value"] == "")): ?> 
            	<a href="" id="togglecontact" value="Toggle Contact Info" name="togglecontact">Information/Contact</a>
			<? endif; ?>
        <?php endif; ?>
	</hgroup>
	<?php  if (strstr($node->path, '/faculty') && strstr($node->path, 'division/')): ?>
	<?php else:?>
    	<?php print $sidebar; ?>
	<?php  endif; ?>

    <?php if ($breadcrumb || $title || $mission || $messages || $help || $tabs): ?>
    	<?php print $breadcrumb; ?>
    <?php endif; ?>
    <div id="content-area">
    	<?php print $content; ?>
    </div> <!-- /#content-area -->
   <!-- 
    <?php if ($right): ?>
      <div id="sidebar-second" class="column sidebar second">
        <div id="sidebar-second-inner" class="inner">
          <?php print $right; ?>
        </div>
      </div>
    <?php endif; ?>  /sidebar-second -->
    </div> 
  </div>
</div>
<!-- /main -->
<!-- ______________________ FOOTER _______________________ -->
<div class="wrapperfooter">
	<footer>
		<?php if(!empty($footer_message) || !empty($footer_block)): ?>
        <div id="footer">
          <?php print $footer_message; ?>
          <?php print $footer_block; ?>
        </div> <!-- /footer -->
      <?php endif; ?>
	</footer>
	<nav>
	</nav>
</div>
<!-- JavaScript at the bottom for fast page loading -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php print $base_path . path_to_theme(); ?>/js/libs/slides.min.jquery.js"></script>
<script type="text/javascript" src="http://use.typekit.com/saw7umj.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<!-- scripts concatenated and minified via build script -->
<script type="text/javascript">
$(document).ready(function() 
{ 
	$(".group-contactinfo").hide();
	$('#togglecontact').click(function(){
	$('.group-contactinfo').fadeToggle("slow", "swing");
		return false;
		});
		});
</script>
<script>
		$(function(){
			$('#slides').slides({
				preload: true,
				generatePagination: false,
				play: 4000
			});
		});
	</script>
 <script>
  $(document).ready(function() {
    $(".accordion").accordion({ 
		collapsible: true,
		active: false,
		autoHeight: false,
		header: 'h3, p'
	  });
  });
  </script>
    
  <!-- end scripts -->

  <!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
       mathiasbynens.be/notes/async-analytics-snippet -->
  <script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>

<?php print $closure; ?>
</body>
</html>