    <?php
        if (
            (($block->module == "block") && (user_access('administer blocks'))) ||
            (($block->module == "views") && (user_access('administer views')))
        ):
    ?>

        <div class="administer">
            <a href="<?php print check_url(base_path()) ?>admin/build/block/configure/<?php print $block->module;?>/<?php print $block->delta;?>">Edit this block</a>
        </div>
    <?php endif; ?>