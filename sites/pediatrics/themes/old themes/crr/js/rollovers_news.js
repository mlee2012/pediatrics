		
	$(document).ready(function() {
		
		// Preload all rollovers
		$("#mainNews img").each(function() {
			// Set the original src
			rollsrc = $(this).attr("src");
			rollON = rollsrc.replace(/.jpg$/ig,"_over.jpg");
			$("<img>").attr("src", rollON);
		});
		
		// Navigation rollovers
		$("#mainNews a").mouseover(function(){
			imgsrc = $(this).children("img").attr("src");
			matches = imgsrc.match(/_over/);
			
			// don't do the rollover if state is already ON
			if (!matches) {
			imgsrcON = imgsrc.replace(/.jpg$/ig,"_over.jpg"); // strip off extension
			$(this).children("img").attr("src", imgsrcON);
			}
			
		});
		$("#mainNews a").mouseout(function(){
			$(this).children("img").attr("src", imgsrc);
		});
		
	
	});
	
		
	
		
	