// SHOW
function show(elementToShow) {
	$('#' + elementToShow).show();
}


// BULLET NEWS SWITCHER
function activateSwitcher() {
	$('.bulletContent_news').cycle({
		fx:      'fade',
		speed:   '300',
		timeout:  4000,
		next: '.bulletSwitcher',
		pager:  '.bulletSwitcher',
		pause: 1
	});
}


