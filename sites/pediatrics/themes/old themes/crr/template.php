<?php
function phptemplate_preprocess_page(&$vars) {
        
    if (module_exists('path')) {
    
        // following was removing edit from the URL path, causing a template to be served up
        // this is undesirable in some cases because it throws off the editing template
        //$alias = drupal_get_path_alias(str_replace('/edit','',$_GET['q']));
        
        $alias = drupal_get_path_alias($_GET['q']);

        if ($alias != $_GET['q']) {
            $template_filename = 'page';

            foreach (explode('/', $alias) as $path_part) {
                $template_filename = $template_filename . '-' . $path_part;
                $vars['template_files'][] = $template_filename;
                //echo "<!-- BEGIN custom function ".$_GET['q']." - ".$template_filename." -->";
            }

//             echo "<!--";
//             print_r( $vars['template_files'] );
//             echo "-->";

        }
    
    }

}


function phptemplate_preprocess_node(&$variables) {
  $node = $variables['node'];
  if (module_exists('taxonomy')) {
    $variables['taxonomy'] = taxonomy_link('taxonomy terms', $node);
  }else {
    $variables['taxonomy'] = array();
  }

  if ($variables['teaser'] && $node->teaser) {
    $variables['content'] = $node->teaser;
  }
  elseif (isset($node->body)) {
    $variables['content'] = $node->body;
  }
  else {
    $variables['content'] = '';
  }

  $variables['date']      = format_date($node->created);
  $variables['links']     = !empty($node->links) ? theme('links', $node->links, array('class' => 'links inline')) : '';
  $variables['name']      = theme('username', $node);
  $variables['node_url']  = url('node/'. $node->nid);
  $variables['terms']     = theme('links', $variables['taxonomy'], array('class' => 'links inline'));
  $variables['title']     = check_plain($node->title);

  // Flatten the node object's member fields.
  $variables = array_merge((array)$node, $variables);

  // Display info only on certain node types.
  if (theme_get_setting('toggle_node_info_'. $node->type)) {
    $variables['submitted'] = theme('node_submitted', $node);
    $variables['picture'] = theme_get_setting('toggle_node_user_picture') ? theme('user_picture', $node) : '';
  } else {
    $variables['submitted'] = '';
    $variables['picture'] = '';
  }
  // Clean up name so there are no underscores.
  //$variables['template_files'][] = 'node-'. $node->type;

  
  // modification to display custom node template depending on the path
  //$variables['template_files'][] = 'node-flu';
  
        $alias = drupal_get_path_alias($_GET['q']);

        if ($alias != $_GET['q']) {
            $template_filename = 'node';

            foreach (explode('/', $alias) as $path_part) {
                $template_filename = $template_filename . '-' . $path_part;
                $variables['template_files'][] = $template_filename;
                //echo "<!-- BEGIN custom function ".$_GET['q']." - ".$template_filename." -->";
            }

        }else{

            $variables['template_files'][] = 'node-'. $node->type;

        }

//             echo "<!-- BEGIN node preprocess ";
//             print_r( $node );
//             echo "-->";  
               
}

function crr_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));

  if (!empty($extra_class)) {
    $class .= ' ' . $extra_class;
  }

  if ($in_active_trail) {
    $class .= ' active-trail';
  }

  if (module_exists(pathauto)) {
    if(!function_exists('pathauto_cleanstring')) {
      _pathauto_include();
    }

    $class .= ' ' . pathauto_cleanstring($link['title']);
  }

  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }

  $href = l($link['title'], $link['href'], $link['localized_options']);

  return '<li class="' . $class . '">' . $href . $menu . "</li>\n";
}

function crr_menu_item_link($link) {
  return $link;
}

?>