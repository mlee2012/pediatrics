<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?><?php print $scripts; ?>
<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>
<body class="<?php print $body_classes; ?>">
<div id="page">
  <div id="header">
     <?php if($search_area): ?>
    <div id="search-wrapper"> <?php print $search_area; ?> </div>
    <?php endif; ?>
    <div id="utility-block" class="clear-block"> <?php print $top_navigation; ?> </div>


     <div id="logo-title">
      <?php if (!empty($logo)): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"> <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /> </a>
      <?php endif; ?>
      <?php if($site_name || $site_slogan): ?>
      <div class="branding">
        <?php if ($site_name): ?>
        <h2><a href="<?php print $front_page; ?>" title="<?php print $site_name ?>"><?php print $site_name ?></a></h2>
        <?php endif; ?>
        <?php if ($site_slogan): ?>
        <span><?php print $site_slogan ?></span>
        <?php endif; ?>
      </div>
      <?php endif; ?>
    </div>    <!-- /logo-title -->

    <?php if (!empty($header)): ?>
    <div id="header-region"> <?php print $header; ?> </div>
    <?php endif; ?>
    <div class="clearBoth"></div>
    <?php if (!empty($secondary_links)): ?>
    <div id="secondary" class="clear-block"> <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?> </div>
    <?php endif; ?>

    <!-- /navigation -->
  </div>
  <!-- /header -->
   <!--<div id="navigation" class="menu <?php if (!empty($primary_links)) { print "withprimary"; } if (!empty($secondary_links)) { print " withsecondary"; } ?> ">-->
   <div id="navigation" class="menu">
      <?php if (!empty($main_navigation)): ?>
      <div class="clear-block"><?php print $main_navigation ?></div>
      <?php endif; ?>
    </div>
  <div id="container" class="clear-block">
   <div id="content-wrapper" class="clearfix">

   		 <?php if ($left_content): ?>
            <div id="left-section-block"><?php print $left_content; ?></div>
        <?php endif; ?>


           		 <?php if ($top_content): ?>
            <?php print $top_content; ?>
        <?php endif; ?>

		<div id="main">
        <?php if ($title): ?>
          <h1><?php echo $title; ?></h1>
        <?php endif; ?>

        <?php print $content; ?>
        </div>

      <div class="clear"></div>
    </div>
  </div>

  <div id="tier-container" class="clear-block"> <?php print $tier_content; ?> </div>
  <!-- /container -->
  <!-- /footer -->
  <div id="footer-content-wrapper"> <?php print $footer_message; ?>
    <?php if (!empty($footer)): print $footer; endif; ?>
  </div>
  <!-- /footer-wrapper -->
  <?php print $closure; ?>
<!-- /page -->
</body>
</html>
