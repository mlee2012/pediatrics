<?php ?>
<!doctype html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title></title>
  <meta name="description" content="">

  <!-- Mobile viewport optimized: h5bp.com/viewport -->
  <meta name="viewport" content="width=device-width">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->

  
  
  <title><?php print $head_title; ?></title>
<?php print $head; ?><?php print $styles; ?><?php print $setting_styles; ?>
<!--[if IE 8]>
  <?php print $ie8_styles; ?>
  <![endif]-->
<!--[if IE 7]>
  <?php print $ie7_styles; ?>
  <![endif]-->
<!--[if lte IE 6]>
  <?php print $ie6_styles; ?>
  <![endif]-->
<?php print $local_styles; ?><?php print $scripts; ?>
</head>

<body id="<?php print $body_id; ?>" class="<?php print $body_classes; ?>">
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
  


  
    <a href="#maincontent" class="skip">Skip to Main Content</a>
    
    
    
    <nav class="metanav">
		 <?php print theme('grid_row', $header_top, 'header-top', 'full-width', $grid_width); ?> <?php print $header; ?>
	</nav>
	<div class="wrapperheader">
	  <header class="pageheader">
		<hgroup>
			<?php if ($logo || $site_name || $site_slogan): ?>
			 <?php if ($logo): ?>
			<h1 class="ir pnslogo">Columbia University College of Physicians and Surgeons</h1>
              <?php endif; ?>
              <?php if ($site_name || $site_slogan): ?>
                <?php if ($site_name): ?>
               <a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><h2 class="orgname"><?php print $site_name; ?></h2></a>
                <?php endif; ?>
                <?php if ($site_slogan): ?>
                <span id="slogan"><?php print $site_slogan; ?></span>
                <?php endif; ?>
              <!-- /site-name-wrapper -->
              <?php endif; ?>
          <?php endif; ?>
			
		</hgroup>
	  </header>
  </div>  
	  
   <div class="wrappermain clearfix">
       <nav class="primarynav">
		 <?php print theme('grid_block', $primary_links_tree, 'primary-menu'); ?>
	</nav>
       <nav class="secondarynav">
		<?php print theme('grid_row', $sidebar_first, 'sidebar-first', 'nested', $sidebar_first_width); ?>
	
		<ul class="relatedlinks">
			<li><h2>Related Links</h2></li>
			<li><a href="">Biomathematics</a></li>
			<li><a href="">Blood and Marrow Transplantation</a></li>
			<li><a href="">Cardiology</a></li>
			<li><a href="">Child and Adolescent Health</a></li>
			<li><a href="">Clinical Genetics</a></li>
		</ul>	
	</nav>
       
    
          

         
   <div id="maincontent" role="main">
	    <hgroup>
   <?php print theme('grid_block', $tabs, 'content-tabs'); ?>
                          
                            
                              <?php if ($title): ?>
                              
                              <h1 class="title"><?php print $title; ?> </h1>
                              <a href="" id="togglecontact" value="Toggle Contact Info" name="togglecontact">Information/Contact</a>
							<div style="display: block;" id="infocontact">
							<h2>Allergy and Immunology Information</h2>
							<dl>
								<dt> Director </dt>
								<dd> David Resnick, M.D. </dd>
								<dt> Administrator </dt>
								<dd> Allison Lyones-Ankeny, J.D. </dd>
								<dt> Phone </dt>
								<dd> (212) 305-3381 </dd>
								<dt> Administrative Coordinator </dt>
								<dd> Paula Colangrande </dd>
								<dt> Phone </dt>
								<dd> (212) 305-2300 </dd>
								<dt> Fax </dt>
								<dd> (212) 305-4538 </dd>
							</dl>
							</div>


                             
                              <?php endif; ?>
                              </hgroup>
                              
    <article>
    <?php if ($content): ?>
	  <?php print $content; ?> 
	  <?php endif; ?>
   	</article>
    </div>
   </div>
    </div> 
    <div class="wrapperfooter">
		<footer>	
    <?php print theme('grid_row', $footer, 'footer', 'full-width'); ?>
		</footer>
		<nav>
		</nav>
	</div>
   
   
   
   
    <?php print theme('grid_row', $preface_top, 'preface-top', 'full-width', $grid_width); ?> <?php print theme('grid_row', $preface_bottom, 'preface-bottom', 'nested'); ?>
      


                  
                    

      <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.min.js"><\/script>')</script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/jquery-ui.min.js"></script>

  <!-- scripts concatenated and minified via build script -->
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>
  <script type="text/javascript">
	     $(document).ready(function() { 
	 	   $("#infocontact").hide();
		   $('#togglecontact').click(function(){
			 $('#infocontact').fadeToggle("slow", "swing");
			 return false;
		   });
		 });




	</script>
  
  <!-- end scripts -->

  <!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
       mathiasbynens.be/notes/async-analytics-snippet -->
  
  
  
  <script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>
</body>
</html>                  
                        
  
<?php print $closure; ?>



</body>
</html>
