<?php ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">
<head>
<title><?php print $head_title; ?></title>
<?php print $head; ?><?php print $styles; ?><?php print $setting_styles; ?>
<!--[if IE 8]>
  <?php print $ie8_styles; ?>
  <![endif]-->
<!--[if IE 7]>
  <?php print $ie7_styles; ?>
  <![endif]-->
<!--[if lte IE 6]>
  <?php print $ie6_styles; ?>
  <![endif]-->
<?php print $local_styles; ?><?php print $scripts; ?>
</head>
<body id="<?php print $body_id; ?>" class="<?php print $body_classes; ?>">
<div id="page" class="page">
  <div id="page-inner" class="page-inner">
    <div id="skip"> <a href="#main-content-area"><?php print t('Skip to Main Content Area'); ?></a> </div>
    <!-- header-top row: width = grid_width --> 
    <?php print theme('grid_row', $header_top, 'header-top', 'full-width', $grid_width); ?> 
    <!-- header-group row: width = grid_width -->
    <div id="header-group-wrapper" class="header-group-wrapper full-width">
      <div id="header-group" class="header-group row <?php print $grid_width; ?>"> <?php print $header; ?>
        <div id="header-group-inner" class="header-group-inner inner clearfix">
          <?php if ($logo || $site_name || $site_slogan): ?>
          <div id="header-site-info" class="header-site-info block">
            <div id="header-site-info-inner" class="header-site-info-inner inner">
              <?php if ($logo): ?>
              <div id="logo"> <a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a> </div>
              <?php endif; ?>
              <?php if ($site_name || $site_slogan): ?>
              <div id="site-name-wrapper" class="clearfix">
                <?php if ($site_name): ?>
                <span id="site-name"><a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a></span>
                <?php endif; ?>
                <?php if ($site_slogan): ?>
                <span id="slogan"><?php print $site_slogan; ?></span>
                <?php endif; ?>
              </div>
              <!-- /site-name-wrapper -->
              <?php endif; ?>
            </div>
            <!-- /header-site-info-inner --> 
          </div>
          <!-- /header-site-info -->
          <?php endif; ?>
          <?php print theme('grid_block', $primary_links_tree, 'primary-menu'); ?> </div>
        <!-- /header-group-inner --> 
      </div>
      <!-- /header-group --> 
    </div>
    <!-- /header-group-wrapper --> 
    <!-- preface-top row: width = grid_width --> 
    <?php print theme('grid_row', $preface_top, 'preface-top', 'full-width', $grid_width); ?> <?php print theme('grid_row', $preface_bottom, 'preface-bottom', 'nested'); ?> 
    <!-- main row: width = grid_width --> 
    
  <!-- /main-wrapper news ticker --> 
  <?php print theme('grid_row', $postscript_top, 'postscript-top', 'nested'); ?> </div>
  <!-- postscript-bottom row: width = grid_width --> 
  <?php print theme('grid_row', $postscript_bottom, 'postscript-bottom', 'full-width', $grid_width); ?> 
  <!-- footer row: width = grid_width --> 
  <?php print theme('grid_row', $footer, 'footer', 'full-width', $grid_width); ?> 
  <!-- footer-message row: width = grid_width -->
  <div id="footer-message-wrapper" class="footer-message-wrapper full-width">
    <div id="footer-message" class="footer-message row <?php print $grid_width; ?>">
      <div id="footer-message-inner" class="footer-message-inner inner clearfix"> <?php print theme('grid_block', $footer_message, 'footer-message-text'); ?> </div>
      <!-- /footer-message-inner --> 
    </div>
    <!-- /footer-message --> 
  </div>
  <!-- /footer-message-wrapper --> 
</div>
<!-- /page-inner -->
</div>
<!-- /page --> 
<?php print $closure; ?>
</body>
</html>
