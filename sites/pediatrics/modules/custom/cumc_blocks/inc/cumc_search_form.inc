<?php
function cumc_blocks_preprocess_cumc_search_form(&$vars, $hook) {
  $vars['search_domains'] = variable_get(CUMC_SEARCH_FORM_SEARCH_DOMAINS_KEY, array());
}
