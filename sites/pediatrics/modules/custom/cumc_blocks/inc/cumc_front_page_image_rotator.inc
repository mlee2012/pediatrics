<?php
function cumc_blocks_preprocess_cumc_front_page_image_rotator(&$vars, $hook) {
  $sql = "SELECT n.nid,
                 n.title,
                 f.filepath
          FROM node n JOIN
               image i ON i.nid = n.nid JOIN
               files f ON f.fid = i.fid
          WHERE n.type = 'image' AND
                n.status = 1 AND
                n.promote = 1 AND
                i.image_size = '_original'
          ORDER BY n.sticky DESC,
                   n.created DESC";

  $result = db_query(db_rewrite_sql($sql));

  $images = array();

  while ($row = db_fetch_array($result)) {
    $images[] = $row;
  }

  $vars['images'] = $images;
}
