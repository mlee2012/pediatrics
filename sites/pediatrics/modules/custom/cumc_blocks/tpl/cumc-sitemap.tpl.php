<? global $base_url, $theme_path; ?>

<div class="clearBoth">&nbsp;</div>
<ul>
  <li><h2><a href="<?=$base_url?>/research">RESEARCH</a></h2>
    <liresearch li=""></liresearch></li>
  <li><a href="http://evpr.columbia.edu/" target="_blank" class="home">Office of the EVP for Research</a></li>
  <li><a href="<?=$base_url?>/research/research-facilities">Research Facilities</a></li>
  <li><a href="<?=$base_url?>/research/inventions-innovations">Inventions and Innovations</a></li>
  <li><a href="<?=$base_url?>/research/research-administration">Research Administration</a>
    <br />     &nbsp;</li>     
  <li><strong>Sponsored Projects:</strong><br />
    <a href="http://spa.columbia.edu/" target="_blank" class="home">Administration</a>&nbsp;| <a href="http://sponsoredprojectshandbook.columbia.edu/" target="_blank" class="home">Handbook</a></li>
  <li><a href="https://www.rascal.columbia.edu/" target="_blank" title="Electronic Res.Admin(RASCAL)" class="home">Electronic Res. Admin(RASCAL)</a></li>
  <li><a href="<?=$base_url?>/research/research-shared-facilities-name" target="_blank" title="Reaserch Resources" class="home">Research Services &amp; Facilities (Cores)</a></li>
  <li class="No"><a href="http://evpr.columbia.edu/" target="_blank" title="More Research" class="home">More &gt;&gt;</a></li>
</ul>

<ul>
  <li><h2><a href="<?=$base_url?>/education">EDUCATION</a></h2></li>
  <li><a href="http://ps.columbia.edu" target="_blank">College of Physicians and Surgeons</a></li>
  <li><a href="http://dental.columbia.edu/" target="_blank">College of Dental Medicine</a></li>
  <li><a href="http://www.mailman.columbia.edu/" target="_blank">Mailman School of Public Health</a></li>
  <li><a href="http://www.nursing.columbia.edu/" target="_blank">School of Nursing</a></li>
  <li><a href="http://sklad.cumc.columbia.edu/gsas/" target="_blank">Graduate School (Basic Science)</a></li>
  <li><a href="<?=$base_url?>/education/more_education">More About Education</a></li>
</ul>

<ul>
  <li><h2><a href="<?=$base_url?>/patient-care">PATIENT CARE</a></h2></li>
  <li><a href="<?=$base_url?>/patient-care/findhealthcare">Find a Healthcare Provider</a></li>
  <li><a href="<?=$base_url?>/patient-care/patient_resources">Patient Resources</a></li>
  <li><a href="http://www.columbiaclinicaltrials.org/" target="_blank">Clinical Trials Office</a></li>
  <li><a href="http://www.cumc.columbia.edu/hipaa/index.html" target="_blank">HIPAA</a></li>
  <li><a href="<?=$base_url?>/patient-care/hw_affiliates">Hospital Affiliates</a></li>
  <li><a href="<?=$base_url?>/patient-care/doctor_patient_services">Online Doctor-Patient Services</a></li>
</ul>

<ul>
  <li><h2><a href="<?=$base_url?>/about">ABOUT US</a></h2></li>
  <li><a href="<?=$base_url?>/about/services-resources">Services &amp; Resources</a></li>
  <li><a href="<?=$base_url?>/about/administration">Administration</a></li>
  <li><a href="<?=$base_url?>/about/departments">A to Z Lising</a></li>
  <li><a href="<?=$base_url?>/about/contact-us">Contact Us</a></li>
  <li><a href="<?=$base_url?>/map">Maps &amp; Directions</a></li>
  <li><a href="https://jobs.columbia.edu/">Jobs</a></li>
  <li><a href="<?=$base_url?>/about/development-alumni">Development &amp; Alumni</a></li>
  <li><a href="<?=$base_url?>/about/donate">Donate</a></li>
</ul>

<ul>
  <li><h2><a href="<?=$base_url?>/news-events">NEWSROOM</a></h2></li>
  <li><a href="<?=$base_url?>/news">News</a></li>
  <li><a href="<?=$base_url?>/press-releases">Press Releases</a></li>
  <li><a href="<?=$base_url?>/features">Features</a></li>
  <li><a href="<?=$base_url?>/news/publications">Publications</a></li>
  <li><a href="http://www.cumc.columbia.edu/celebrates/">CUMC Celebrates</a></li>
  <li><a href="<?=$base_url?>/events">Events</a><br />
    &nbsp;</li>
</ul>
