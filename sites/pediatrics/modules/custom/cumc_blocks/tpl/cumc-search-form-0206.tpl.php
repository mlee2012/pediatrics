<!-- Search Area -->
<form action="http://search.cumc.columbia.edu/search" method="get" name="myform" id="cumc-search-form">
  <input type="hidden" name="site" value="" />
  <input type="hidden" name="output" value="xml_no_dtd" />
  <input type="hidden" name="lr" value="" />
  <input type="hidden" name="oe" value="" />
  <input type="hidden" name="qt" value="" />
  <input type="hidden" name="q" value="" />
  <input type="hidden" name="client" value="cumc" />
  <input type="hidden" name="proxystylesheet" value="cumc" />
  <input type="hidden" name="filter.searchTerm" value="" />
  <input type="hidden" name="fullname" value="" />
  <input type="hidden" name="type" value="test" />

  <fieldset class="search-fields">
    <legend>Search</legend>
    <input type="image" height="26" border="0" value="searchbox" src="<?=$theme_path?>/img/icon/search.png" alt="Search" class="form-submit" id="cumc-search-form-submit" />
    <input type="text" maxlength="60" name="searchbox" size="26"  value="" class="form-text" id="cumc-search-form-text" />
  </fieldset>

  <?php if (count($search_domains) > 1): $i = 0; ?>
    <fieldset class="search-domains">
      <legend>Domains</legend>

      <?php foreach ($search_domains as $domain): ?>
        <input type="radio" name="searchdomain" id="search-domain-<?php echo $domain; ?>" value="<?php echo $domain; ?>" <?php if ($i++ == 0): ?>checked="checked" <?php endif; ?>/> <label for="search-domain-<?php echo $domain; ?>"><?php echo $domain; ?></label>

      <?php endforeach; ?>
    </fieldset>
  <?php elseif (count($search_domains) == 1): ?>
    <input type="hidden" name="searchdomain" id="search-domain-<?php echo $search_domains[0]; ?>" value="<?php echo $search_domains[0]; ?>" checked="checked" />
  <?php endif; ?>
</form>

<!-- END OF: Search Area -->
