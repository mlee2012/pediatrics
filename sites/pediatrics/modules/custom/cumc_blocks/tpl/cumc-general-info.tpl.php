<ul>
  <li>General Information: 212 305-CUMC</li>
  <li><a href="http://www.columbia.edu/help/copyright.html" target="_blank">&copy; 2010 Columbia University</a></li>
  <li><a href="mailto:rsb2148@columbia.edu?subject=CUMC Website Comment">Comments</a></li>
  <li><a href="mailto:dyoun@columbia.edu?subject=CUMC Website Issue">Contact Webmaster</a></li>
  <li><a href="http://wds.cumc.columbia.edu/" target="_blank">Built by Web Design Studio</a></li>
</ul>
