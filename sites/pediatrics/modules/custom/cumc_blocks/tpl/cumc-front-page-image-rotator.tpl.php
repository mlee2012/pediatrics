<?php if ($images): ?>
  <ul>
    <?php foreach ($images as $image): ?>
      <li><img src="<?php echo $base_path; ?><?php echo $image['filepath']; ?>" alt="<?php echo $image['title']; ?>" />
    <?php endforeach; ?>
  </ul>
<?php endif; ?>
