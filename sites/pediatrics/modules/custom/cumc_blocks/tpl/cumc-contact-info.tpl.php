<div class="vcard">
  <a class="fn org url" href="http://www.commerce.net/">Department of Radiology</a>

  <div class="adr">
    <div class="street-address">180 Fort Washington Avenue, 3rd Floor</div>
    <div class="extended-address">Harkness Pavillion, Room 313</div>
    <div>
      <span class="locality">New York</span>,
      <abbr class="region">NY</abbr>
      <span class="postal-code">10032</span>
    </div>
    <div class="country-name">U.S.A.</div>
  </div>

  <span class="tel work-tel">
    <span class="type">Work</span> Tel:
    <span class="value">(212) 305-1948</span>
  </span>

  <span class="tel fax">
    <span class="type">Fax</span>:
    <span class="value">(212) 305-5777</span>
  </span>
  
  <p class="hcard">This content uses <a rel="profile" href="http://microformats.org/profile/hcard">hCard</a>.</p>
</div>
