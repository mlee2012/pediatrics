jQuery(function($) {
  var cumc_search_form = $('#cumc-search-form');

  if (cumc_search_form) {
    cumc_search_form[0].reset();
    $('#cumc-search-form .form-text').removeAttr('disabled');

    cumc_search_form.submit(function() {
      var inputs = $('#cumc-search-form :input');

      switch ($('input[name=searchdomain]:checked').val()) {
        case 'cumc': // p&s google search
          document.myform.action = 'http://search.cumc.columbia.edu/search';
          document.myform.client.value = "cumc";
          document.myform.site.value = "cumc";
          document.myform.proxystylesheet.value = "cumc";
          document.myform.q.value = document.myform.searchbox.value;

          document.myform.searchdomain.disabled = true;
          document.myform.searchbox.disabled = true;
          document.myform.fullname.disabled = true;
          break;
        case 'pediatrics': // google search on morningside
          document.myform.action = 'http://search.columbia.edu/search?';
          document.myform.client.value = "pediatrics";
          document.myform.site.value = "Columbia";
          document.myform.proxystylesheet.value = "columbia";
          document.myform.q.value = document.myform.searchbox.value;

          document.myform.searchdomain.disabled = true;
          document.myform.searchbox.disabled = true;
          document.myform.fullname.disabled = true;
          break;
        case 'people': // people search
          document.myform.action = 'https://directory.columbia.edu/people/search?';
          document.myform.fullname.value = document.myform.searchbox.value;
          document.myform.elements['filter.searchTerm'].value = document.myform.searchbox.value;
          document.myform.type.value = "lookup";

          // filter out google search terms before transmitting form
          document.myform.client.disabled = true;
          document.myform.site.disabled = true;
          document.myform.proxystylesheet.disabled = true;
          break;
        default:
          return false;
      }
    });
  }

  var cumc_front_page_image_rotator = $('#block-cumc_blocks-cumc_front_page_image_rotator');

  if (cumc_front_page_image_rotator) {
    var cumc_front_page_image_rotator_images = $('#block-cumc_blocks-cumc_front_page_image_rotator img');

    if (cumc_front_page_image_rotator_images.length > 1) {
      var cumc_front_page_image_rotator_image_index = 0;

      setInterval(function() {
        $(cumc_front_page_image_rotator_images[cumc_front_page_image_rotator_image_index]).fadeOut('slow');

        cumc_front_page_image_rotator_image_index++;

        if (cumc_front_page_image_rotator_image_index >= cumc_front_page_image_rotator_images.length) {
          cumc_front_page_image_rotator_image_index = 0;
        }

        $(cumc_front_page_image_rotator_images[cumc_front_page_image_rotator_image_index]).fadeIn('slow');
      }, 6000);
    }
  }
});
