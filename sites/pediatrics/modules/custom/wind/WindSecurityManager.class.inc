<?php 
  class WindSecurityManager {
	var $settings = array();
	var $wind_server = "wind.columbia.edu";
	var $wind_login_uri = "/login";
	var $wind_validate_uri = "/validate";
	var $UNI;
	var $kitten_loggedin;
	var $kitten_email;
	var $kitten_userlevel;
	var $login_error;

	public function __construct($settings) { 
		session_start();
		$this->settings = $settings; 
	}

	function isAuthorized() {
	  if ($_SESSION["kitten_loggedin"] == TRUE && $_SESSION["UNI"])
		return true;

	  $wget_args = "https://$this->wind_server$this->wind_validate_uri?ticketid=";

	  $this->UNI = 0; // Your Columbia Universal Network Identifier
	  $this->kitten_email = "";
	  $this->kitten_loggedin = FALSE;
	  $this->kitten_userlevel = 0;
	
	  list($this->UNI, $this->kitten_email, $this->kitten_userlevel, $this->kitten_loggedin) = $this->refresh_session_variables();

	  //  BEGIN LOGIN PROCESSING if necessary
	  if ($_GET["ticketid"]) {   // If the user wants to login, check to see if they have a ticket.
		// If they have a ticket, validate it against the wind server.
		$wget_args = $wget_args . $_GET["ticketid"];
		list($firstline , $secondline) = $this->wrap_wget($wget_args);
		if($firstline == "no")
		  $authenticated=FALSE;
		else if($firstline=="yes") {
		  $authenticated=TRUE;
		  $this->UNI = $secondline;
		}
	  }
	  else
		$this->login_form();

	  // At this point, if authentication was successful, $authenticated and $UNI are set
	  // If unsuccessful, $authenticated is false.
	  if ($authenticated == TRUE) {
		$this->kitten_loggedin = TRUE;
		$this->kitten_email = $this->UNI . "@columbia.edu";
		$this->kitten_userlevel = 1;
	
	    /* Permitted Users function not in use currently
		if (isset($this->settings['permittedUsers'])) {
		  if (!in_array($this->UNI,$this->settings['permittedUsers']))
		    // Deny access if not authorized
		    return false;
		}
	    */
	
	    $_SESSION["UNI"] = $this->UNI;
	    $_SESSION["kitten_email"] = $this->kitten_email;
	    $_SESSION["kitten_userlevel"] = $this->kitten_userlevel;
	    $_SESSION["kitten_loggedin"] = $this->kitten_loggedin;
	
	    return true;
	  }
	
	  return false;
	}
	
	
	// Simple login form
	function login_form() {
	  $wind_realm = $this->settings["windService"];
	
	  if($_SERVER["SERVER_PORT"] == 443)
		$server_protocol = "https";
	  else
		$server_protocol = "http";
	
	  if ($wind_destination_path == "")
	  	$wind_destination_path = $_GET["wind_destination_path"];

	  $path = "user/wind";
	  
	  if ($wind_destination_path)
	  	$path = $path . "?wind_destination_path=" . $wind_destination_path;

	  $destination = $server_protocol . "://" . $_SERVER["SERVER_NAME"] . base_path() . $path;
	
	  $login_link = "https://$this->wind_server$this->wind_login_uri?service=$wind_realm&destination="
		. urlencode($destination);
	
	  if ($this->kitten_loggedin) {
		session_destroy();
		header("Location: $login_link");
		echo "You are currently logged in as <b>$this->kitten_email</b><br />";
	  }
	  else
		header("Location: $login_link");
	}
	
	
	// Refresh the session variables into the current scope.
	function refresh_session_variables() {
	  if(!isset($_SESSION["kitten_email"]))
		$_SESSION["kitten_email"] = "";
	  if(!isset($_SESSION["UNI"])) {
		$_SESSION["UNI"] = 0;
	  }
	  if(!isset($_SESSION["kitten_userlevel"]))
		$_SESSION["kitten_userlevel"] = 0;
	
	  $this->UNI = $_SESSION["UNI"];
	  $this->kitten_email = $_SESSION["kitten_email"];
	  $this->kitten_userlevel = $_SESSION["kitten_userlevel"];
	
	  if($this->kitten_userlevel > 0)
		$this->kitten_loggedin = TRUE;
	
	  return array($this->UNI, $this->kitten_email, $this->kitten_userlevel, $this->kitten_loggedin);
	}
	
	function wrap_wget($args) {	
		$curli = curl_init();
		curl_setopt($curli, CURLOPT_URL, $args);
		curl_setopt($curli, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt ($curli, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curli, CURLOPT_CAINFO, '/usr/share/ssl/certs/intermediateCAbundle.crt');
		$output_array = explode("\n", curl_exec ($curli));
			
	  return( array($output_array[0], $output_array[1], $output_array[2]));
	}
	
  }
?>