WIND
=============

WIND is a web authentication system which will verify that a person using a web
browser can authenticate using a UNI. This module will authenticate a user via
the Columbia University WIND authentication service.  It also auto-creates
Drupal-native accounts if desired, facilitates bulk user creation, assigns
roles, and supports LDAP entry retrieval and user profile support.

Requirements
------------

   PHP >= 5.2.5 with the following modules installed:
   curl, openssl, dom, zlib, and xml

Installation
------------

    Place the wind folder in your Drupal modules directory.

    Go to Administration > Modules and enable this module.

    Go to Administration > Configuration > People > WIND to configure the WIND module.