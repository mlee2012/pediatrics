if (Drupal.jsEnabled) {
  $(document).ready(function() {
  	if ($("body.not-logged-in").length) {
  	  if (!(element = Drupal.settings.wind.element))
  	  	return;
  	  var base_path = Drupal.settings.wind.basePath;
  
  	  $(element).mouseover(function() { 
  	  	$(this).css("cursor","pointer"); 
  	  	$(this).attr("title","Click to log in"); 
  	  	$(this).dblclick(function() { 
  	  		document.location = base_path + "user/wind"; 
  	  	});
  	  });
  	}

    // Enable background-image caching for IE6
    var IE6 = /MSIE 6/i.test(navigator.userAgent);
    if (IE6) {
      try {
        document.execCommand("BackgroundImageCache", false, true);
      } catch(err) {}
    }
  });
}