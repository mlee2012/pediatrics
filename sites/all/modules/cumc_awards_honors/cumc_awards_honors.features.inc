<?php

/**
 * Implementation of hook_node_info().
 */
function cumc_awards_honors_node_info() {
  $items = array(
    'award_honor' => array(
      'name' => t('Awards & Honors'),
      'module' => 'features',
      'description' => t('Celebrates Awards/Honors'),
      'has_title' => '1',
      'title_label' => t('Name as You Would Like It to Appear'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
    'grant_and_award' => array(
      'name' => t('Create a Listing for Grant/Renewal/Supplement/Contract'),
      'module' => 'features',
      'description' => t('Celebrates content'),
      'has_title' => '1',
      'title_label' => t('Project Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_rules_defaults().
 */
function cumc_awards_honors_rules_defaults() {
  return array(
    'rules' => array(
      'rules_awards_honors' => array(
        '#type' => 'rule',
        '#set' => 'event_node_insert',
        '#label' => 'Awards/Honors',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'awards',
          '1' => 'honors',
          'cumc_awards_honors' => 'cumc_awards_honors',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#weight' => 0,
            '0' => array(
              '#type' => 'AND',
              '#weight' => 0,
            ),
            '#type' => 'OR',
          ),
          '1' => array(
            '#type' => 'condition',
            '#settings' => array(
              'type' => array(
                'award_honor' => 'award_honor',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#name' => 'rules_condition_content_is_type',
            '#info' => array(
              'label' => 'Created content is Awards/Honors',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content',
                ),
              ),
              'module' => 'Node',
            ),
            '#weight' => 0,
          ),
          '2' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'User has role(s)',
              'arguments' => array(
                'user' => array(
                  'type' => 'user',
                  'label' => 'User',
                ),
              ),
              'module' => 'User',
            ),
            '#name' => 'rules_condition_user_hasrole',
            '#settings' => array(
              'roles' => array(
                '0' => 1,
              ),
              'operation' => 'OR',
              '#argument map' => array(
                'user' => 'user',
              ),
            ),
            '#type' => 'condition',
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#weight' => 0,
            '#type' => 'action',
            '#settings' => array(
              'path' => 'award-honor/thankyou',
              'query' => '',
              'fragment' => '',
              'force' => 0,
              'immediate' => 0,
            ),
            '#name' => 'rules_action_drupal_goto',
            '#info' => array(
              'label' => 'Awards/Honors Confirmation',
              'label callback' => FALSE,
              'module' => 'System',
              'eval input' => array(
                '0' => 'path',
                '1' => 'query',
                '2' => 'fragment',
              ),
            ),
          ),
          '1' => array(
            '#weight' => 0,
            '#type' => 'action',
            '#settings' => array(
              'to' => 'kb2453@columbia.edu,dyoun@columbia.edu,mjs2219@mail.cumc.columbia.edu',
              'from' => 'CUMC Awards & Honors<no-reply@columbia.edu>',
              'subject' => 'New Awards/Honors',
              'message' => 'A new Awards/Honors form has been submitted. [node:title] --  [node:node-url] ',
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'message' => array(
                    '0' => 'node',
                  ),
                ),
              ),
            ),
            '#name' => 'rules_action_mail',
            '#info' => array(
              'label' => 'Send a mail to an arbitrary mail address',
              'module' => 'System',
              'eval input' => array(
                '0' => 'subject',
                '1' => 'message',
                '2' => 'from',
                '3' => 'to',
              ),
            ),
          ),
          '2' => array(
            '#type' => 'action',
            '#settings' => array(
              'to' => '[node:field_form_user_email-raw]',
              'from' => 'CUMC Awards & Honors<no-reply@columbia.edu>',
              'subject' => 'CUMC  Awards & Honors Submission',
              'message' => 'Thank you. Your submission will be forwarded to the P&S Department of Communications for consideration for publication. A copy of your submission is in the body of this email. If you have any questions or need to make changes to your submission, please contact Mary Schiller, mjs2219@columbia.edu.     

Name as You Would Like It to Appear:
 [node:title]
Primary CUMC Institution: 
 [node:field_school-raw]
Other Institution: 
 [node:field_institution_other-raw]
Recipient’s Highest Degree(s): 
 [node:field_celebrate_honor_degrees-raw]
Full Faculty Title(s), Including School(s) (Primary Appointment Listed First): 
 [node:field_celebrate_honor-raw]
Full Name or Description of the Honor: 
 [node:field_celebrate_honor_name-raw]
Name of Organization Bestowing the Honor: 
 [node:field_celebrate_honor_award-raw]
Brief Description of the Honor: 
 [node:field_celebrate_honor_descripton-raw]
Date:
 [node:field_date-value]
Notes (for internal use only, not for publication): 
 [node:field_from_notes-raw]
Your Name: 
 [node:field_form_user_name-raw]
Email : 
 [node:field_form_user_email-raw]',
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'to' => array(
                    '0' => 'node',
                  ),
                  'message' => array(
                    '0' => 'node',
                  ),
                ),
              ),
            ),
            '#name' => 'rules_action_mail',
            '#info' => array(
              'label' => 'Send a mail to an arbitrary mail address',
              'module' => 'System',
              'eval input' => array(
                '0' => 'subject',
                '1' => 'message',
                '2' => 'from',
                '3' => 'to',
              ),
            ),
            '#weight' => 0,
          ),
        ),
        '#version' => 6003,
      ),
      'rules_celebrates_rules' => array(
        '#type' => 'rule',
        '#set' => 'event_node_insert',
        '#label' => 'Celebrates Rules',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          '0' => 'celebrates',
          'cumc_awards_honors' => 'cumc_awards_honors',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#type' => 'OR',
            '0' => array(
              '#weight' => 0,
              '0' => array(
                '#info' => array(
                  'label' => 'Created content is A listing Grant and Awards',
                  'label callback' => FALSE,
                  'arguments' => array(
                    'node' => array(
                      'type' => 'node',
                      'label' => 'Content',
                    ),
                  ),
                  'module' => 'Node',
                ),
                '#name' => 'rules_condition_content_is_type',
                '#settings' => array(
                  'type' => array(
                    'grant_and_award' => 'grant_and_award',
                  ),
                  '#argument map' => array(
                    'node' => 'node',
                  ),
                ),
                '#type' => 'condition',
                '#weight' => 0,
              ),
              '#type' => 'AND',
            ),
            '#weight' => 0,
          ),
          '1' => array(
            '#weight' => 0,
            '0' => array(
              '#weight' => 0,
              '#info' => array(
                'label' => 'User has role(s)',
                'arguments' => array(
                  'user' => array(
                    'type' => 'user',
                    'label' => 'User',
                  ),
                ),
                'module' => 'User',
              ),
              '#name' => 'rules_condition_user_hasrole',
              '#settings' => array(
                'roles' => array(
                  '0' => 1,
                ),
                'operation' => 'OR',
                '#argument map' => array(
                  'user' => 'user',
                ),
              ),
              '#type' => 'condition',
            ),
            '#type' => 'OR',
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#type' => 'action',
            '#settings' => array(
              'path' => 'grant-and-award/thankyou',
              'query' => '',
              'fragment' => '',
              'force' => 0,
              'immediate' => 0,
            ),
            '#name' => 'rules_action_drupal_goto',
            '#info' => array(
              'label' => 'Grant and award Confirmation',
              'label callback' => FALSE,
              'module' => 'System',
              'eval input' => array(
                '0' => 'path',
                '1' => 'query',
                '2' => 'fragment',
              ),
            ),
            '#weight' => 0,
          ),
          '1' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Send a mail to an arbitrary mail address',
              'module' => 'System',
              'eval input' => array(
                '0' => 'subject',
                '1' => 'message',
                '2' => 'from',
                '3' => 'to',
              ),
            ),
            '#name' => 'rules_action_mail',
            '#settings' => array(
              'to' => 'kb2453@columbia.edu,dyoun@columbia.edu,mjs2219@mail.cumc.columbia.edu',
              'from' => 'CUMC Awards & Honors<no-reply@columbia.edu>',
              'subject' => 'New Grant/Renewal/Supplement/Contract',
              'message' => 'A new Grant/Renewal/Supplement/Contract  form has been submitted. [node:title] --  [node:node-url] ',
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'message' => array(
                    '0' => 'node',
                  ),
                ),
              ),
            ),
            '#type' => 'action',
          ),
          '2' => array(
            '#weight' => 0,
            '#type' => 'action',
            '#settings' => array(
              'to' => '[node:field_form_user_email-raw]',
              'from' => 'CUMC Awards & Honors<no-reply@columbia.edu>',
              'subject' => 'CUMC  Awards & Honors Submission',
              'message' => 'Thank you. Your submission will be forwarded to the P&S Department of Communications for consideration for publication. A copy of your submission is in the body of this email. If you have any questions or need to make changes to your submission, please contact Mary Schiller, mjs2219@columbia.edu.    

Principal investigator(s):
 [node:title]
Full Faculty Title(s):
 [node:field_faculty_title-raw]
Primary CUMC Institution:
 [node:field_affiliation_other_school-raw]
Primary Department
 [node:field_department-raw]
Second Department(s), If Applicable:
 [node:field_secondary_dept-raw]
Other Institution:
 [node:field_institution_other-raw]
Does the PI have affiliations with other Columbia schools?:
 [node:field_affiliation_other_school-raw]
Additional Institutional Affiliation: 
 [node:field_school2-raw]
Additional Department:
 [node:field_dept_other-raw]
Additional Department Affiliation:
 [node:field_department2-raw]
Grant/Contract Type:
[node:field_grant_type-raw]
Amount:
[node:field_amount-raw]
Length of Grant, Renewal, Supplement:
 [node:field_grant_timeperiod-raw]
Funding Source:
 [node:field_grantsource-raw]
Describe the project funded by this grant or contract:
 [node:field_purpose-raw]
Date Awarded;
 [node:field_date-month] [node:field_date-d], [node:field_date-yyyy]
Additional Principal Investigator?:
 [node:field_radio_add_princip_investe-raw]
Additional Principal Investigators:
 [node:field_pi_co-raw]
Notes (for internal use only, not for publication):
 [node:field_from_notes-raw]
Your Name:
 [node:field_form_user_name-raw]
Email:
 [node:field_form_user_email-raw]
',
              '#eval input' => array(
                'token_rules_input_evaluator' => array(
                  'to' => array(
                    '0' => 'node',
                  ),
                  'message' => array(
                    '0' => 'node',
                  ),
                ),
              ),
            ),
            '#name' => 'rules_action_mail',
            '#info' => array(
              'label' => 'Send a mail to an arbitrary mail address',
              'module' => 'System',
              'eval input' => array(
                '0' => 'subject',
                '1' => 'message',
                '2' => 'from',
                '3' => 'to',
              ),
            ),
          ),
        ),
        '#version' => 6003,
      ),
    ),
  );
}

/**
 * Implementation of hook_views_api().
 */
function cumc_awards_honors_views_api() {
  return array(
    'api' => '2',
  );
}
