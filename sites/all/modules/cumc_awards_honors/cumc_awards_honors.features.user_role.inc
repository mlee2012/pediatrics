<?php

/**
 * Implementation of hook_user_default_roles().
 */
function cumc_awards_honors_user_default_roles() {
  $roles = array();

  // Exported role: anonymous user
  $roles['anonymous user'] = array(
    'name' => 'anonymous user',
  );

  return $roles;
}
