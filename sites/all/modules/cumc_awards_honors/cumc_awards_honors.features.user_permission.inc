<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function cumc_awards_honors_user_default_permissions() {
  $permissions = array();

  // Exported permission: access all views
  $permissions['access all views'] = array(
    'name' => 'access all views',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'site editor',
      '4' => 'wds',
    ),
  );

  // Exported permission: access content
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: access forward
  $permissions['access forward'] = array(
    'name' => 'access forward',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'site editor',
      '4' => 'wds',
    ),
  );

  // Exported permission: create award_honor content
  $permissions['create award_honor content'] = array(
    'name' => 'create award_honor content',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: create grant_and_award content
  $permissions['create grant_and_award content'] = array(
    'name' => 'create grant_and_award content',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_affiliation_other_school
  $permissions['edit field_affiliation_other_school'] = array(
    'name' => 'edit field_affiliation_other_school',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_amount
  $permissions['edit field_amount'] = array(
    'name' => 'edit field_amount',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_celebrate_gift_purpose
  $permissions['edit field_celebrate_gift_purpose'] = array(
    'name' => 'edit field_celebrate_gift_purpose',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_celebrate_gift_towhom
  $permissions['edit field_celebrate_gift_towhom'] = array(
    'name' => 'edit field_celebrate_gift_towhom',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_celebrate_honor
  $permissions['edit field_celebrate_honor'] = array(
    'name' => 'edit field_celebrate_honor',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_celebrate_honor_award
  $permissions['edit field_celebrate_honor_award'] = array(
    'name' => 'edit field_celebrate_honor_award',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_celebrate_honor_degrees
  $permissions['edit field_celebrate_honor_degrees'] = array(
    'name' => 'edit field_celebrate_honor_degrees',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_celebrate_honor_descripton
  $permissions['edit field_celebrate_honor_descripton'] = array(
    'name' => 'edit field_celebrate_honor_descripton',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_celebrate_honor_name
  $permissions['edit field_celebrate_honor_name'] = array(
    'name' => 'edit field_celebrate_honor_name',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_celebrate_image_main
  $permissions['edit field_celebrate_image_main'] = array(
    'name' => 'edit field_celebrate_image_main',
    'roles' => array(
      '0' => 'admin',
      '1' => 'news moderator',
      '2' => 'site editor',
      '3' => 'wds',
    ),
  );

  // Exported permission: edit field_date
  $permissions['edit field_date'] = array(
    'name' => 'edit field_date',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'news moderator',
      '3' => 'site editor',
      '4' => 'wds',
    ),
  );

  // Exported permission: edit field_department
  $permissions['edit field_department'] = array(
    'name' => 'edit field_department',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_department2
  $permissions['edit field_department2'] = array(
    'name' => 'edit field_department2',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_dept_other
  $permissions['edit field_dept_other'] = array(
    'name' => 'edit field_dept_other',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_dept_url
  $permissions['edit field_dept_url'] = array(
    'name' => 'edit field_dept_url',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'news moderator',
      '3' => 'site editor',
      '4' => 'wds',
    ),
  );

  // Exported permission: edit field_faculty_title
  $permissions['edit field_faculty_title'] = array(
    'name' => 'edit field_faculty_title',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_form_user_email
  $permissions['edit field_form_user_email'] = array(
    'name' => 'edit field_form_user_email',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: edit field_form_user_name
  $permissions['edit field_form_user_name'] = array(
    'name' => 'edit field_form_user_name',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: edit field_from_notes
  $permissions['edit field_from_notes'] = array(
    'name' => 'edit field_from_notes',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: edit field_grant_timeperiod
  $permissions['edit field_grant_timeperiod'] = array(
    'name' => 'edit field_grant_timeperiod',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_grant_type
  $permissions['edit field_grant_type'] = array(
    'name' => 'edit field_grant_type',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_grantsource
  $permissions['edit field_grantsource'] = array(
    'name' => 'edit field_grantsource',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_institution_other
  $permissions['edit field_institution_other'] = array(
    'name' => 'edit field_institution_other',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_news_subtitle
  $permissions['edit field_news_subtitle'] = array(
    'name' => 'edit field_news_subtitle',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'news moderator',
      '3' => 'site editor',
      '4' => 'wds',
    ),
  );

  // Exported permission: edit field_pi
  $permissions['edit field_pi'] = array(
    'name' => 'edit field_pi',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_pi_co
  $permissions['edit field_pi_co'] = array(
    'name' => 'edit field_pi_co',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_purpose
  $permissions['edit field_purpose'] = array(
    'name' => 'edit field_purpose',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_radio_add_princip_investe
  $permissions['edit field_radio_add_princip_investe'] = array(
    'name' => 'edit field_radio_add_princip_investe',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_school
  $permissions['edit field_school'] = array(
    'name' => 'edit field_school',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_school2
  $permissions['edit field_school2'] = array(
    'name' => 'edit field_school2',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: edit field_secondary_dept
  $permissions['edit field_secondary_dept'] = array(
    'name' => 'edit field_secondary_dept',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: schedule workflow transitions
  $permissions['schedule workflow transitions'] = array(
    'name' => 'schedule workflow transitions',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'wds',
    ),
  );

  // Exported permission: view date repeats
  $permissions['view date repeats'] = array(
    'name' => 'view date repeats',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: view field_affiliation_other_school
  $permissions['view field_affiliation_other_school'] = array(
    'name' => 'view field_affiliation_other_school',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_amount
  $permissions['view field_amount'] = array(
    'name' => 'view field_amount',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_blurb
  $permissions['view field_blurb'] = array(
    'name' => 'view field_blurb',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: view field_celebrate_gift_purpose
  $permissions['view field_celebrate_gift_purpose'] = array(
    'name' => 'view field_celebrate_gift_purpose',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_celebrate_gift_towhom
  $permissions['view field_celebrate_gift_towhom'] = array(
    'name' => 'view field_celebrate_gift_towhom',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_celebrate_honor
  $permissions['view field_celebrate_honor'] = array(
    'name' => 'view field_celebrate_honor',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_celebrate_honor_award
  $permissions['view field_celebrate_honor_award'] = array(
    'name' => 'view field_celebrate_honor_award',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_celebrate_honor_degrees
  $permissions['view field_celebrate_honor_degrees'] = array(
    'name' => 'view field_celebrate_honor_degrees',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_celebrate_honor_descripton
  $permissions['view field_celebrate_honor_descripton'] = array(
    'name' => 'view field_celebrate_honor_descripton',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_celebrate_honor_name
  $permissions['view field_celebrate_honor_name'] = array(
    'name' => 'view field_celebrate_honor_name',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_celebrate_image_main
  $permissions['view field_celebrate_image_main'] = array(
    'name' => 'view field_celebrate_image_main',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: view field_date
  $permissions['view field_date'] = array(
    'name' => 'view field_date',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: view field_department
  $permissions['view field_department'] = array(
    'name' => 'view field_department',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_department2
  $permissions['view field_department2'] = array(
    'name' => 'view field_department2',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_dept_other
  $permissions['view field_dept_other'] = array(
    'name' => 'view field_dept_other',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_dept_url
  $permissions['view field_dept_url'] = array(
    'name' => 'view field_dept_url',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: view field_event_sundial_url
  $permissions['view field_event_sundial_url'] = array(
    'name' => 'view field_event_sundial_url',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'site editor',
      '4' => 'wds',
    ),
  );

  // Exported permission: view field_expert_area
  $permissions['view field_expert_area'] = array(
    'name' => 'view field_expert_area',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'site editor',
      '4' => 'wds',
    ),
  );

  // Exported permission: view field_faculty_title
  $permissions['view field_faculty_title'] = array(
    'name' => 'view field_faculty_title',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_form_user_email
  $permissions['view field_form_user_email'] = array(
    'name' => 'view field_form_user_email',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: view field_form_user_name
  $permissions['view field_form_user_name'] = array(
    'name' => 'view field_form_user_name',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: view field_from_notes
  $permissions['view field_from_notes'] = array(
    'name' => 'view field_from_notes',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: view field_grant_timeperiod
  $permissions['view field_grant_timeperiod'] = array(
    'name' => 'view field_grant_timeperiod',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_grant_type
  $permissions['view field_grant_type'] = array(
    'name' => 'view field_grant_type',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_grantsource
  $permissions['view field_grantsource'] = array(
    'name' => 'view field_grantsource',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_institution_other
  $permissions['view field_institution_other'] = array(
    'name' => 'view field_institution_other',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_news_subtitle
  $permissions['view field_news_subtitle'] = array(
    'name' => 'view field_news_subtitle',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: view field_orderno
  $permissions['view field_orderno'] = array(
    'name' => 'view field_orderno',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'site editor',
      '4' => 'wds',
    ),
  );

  // Exported permission: view field_pi
  $permissions['view field_pi'] = array(
    'name' => 'view field_pi',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_pi_co
  $permissions['view field_pi_co'] = array(
    'name' => 'view field_pi_co',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_profile
  $permissions['view field_profile'] = array(
    'name' => 'view field_profile',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'site editor',
      '4' => 'wds',
    ),
  );

  // Exported permission: view field_purpose
  $permissions['view field_purpose'] = array(
    'name' => 'view field_purpose',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_radio_add_princip_investe
  $permissions['view field_radio_add_princip_investe'] = array(
    'name' => 'view field_radio_add_princip_investe',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_school
  $permissions['view field_school'] = array(
    'name' => 'view field_school',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_school2
  $permissions['view field_school2'] = array(
    'name' => 'view field_school2',
    'roles' => array(
      '0' => 'anonymous user',
    ),
  );

  // Exported permission: view field_secondary_dept
  $permissions['view field_secondary_dept'] = array(
    'name' => 'view field_secondary_dept',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  // Exported permission: view field_url
  $permissions['view field_url'] = array(
    'name' => 'view field_url',
    'roles' => array(
      '0' => 'admin',
      '1' => 'anonymous user',
      '2' => 'authenticated user',
      '3' => 'news moderator',
      '4' => 'site editor',
      '5' => 'wds',
    ),
  );

  return $permissions;
}
